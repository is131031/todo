from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse, resolve
from django.contrib.auth.models import User
from www.todolist.models import Todo_Entries


# default settings for todolist-tests
class TodoTestCase(TestCase):
    fixtures = ["Testdata.json"]   # 1 user, 5 todo-entries

    def setUp(self):
        # Insertion of a second user
        user = 'testuser'
        password = 'webtech'
        self.user1 = User.objects.get(pk=1)
        self.user2 = User.objects.create_user(
            username=user, password=password)
        # Login of the new inserted user as default user for all tests
        self.client.login(username=user, password=password)


# testing the correct insertion of the testdata
class FixtureTests(TodoTestCase):
    def test_entries(self):
        self.assertEqual(Todo_Entries.objects.all().count(), 6)
        self.assertEqual(User.objects.all().count(), 2)
        # User: 'TodoMaster'
        self.assertEqual(User.objects.get(pk=1), self.user1)
        # User: 'testuser'
        self.assertEqual(User.objects.get(pk=2), self.user2)
        print("FixtureTests executed.")


# Tests to change and insert Data
class Inserts(TodoTestCase):

    def test_data_change(self):
        s = Todo_Entries.objects.get(pk=12)
        self.assertEquals(s.id, 12)
        s.title = 'Letzter Eintrag'
        s.save()
        self.assertEquals(s.title, 'Letzter Eintrag')

    def test_data_insert(self):
        x = Todo_Entries.objects.all().count() + 1
        s = Todo_Entries.objects.create(
            title='XYZ',
            description='XYZ-Description',
            duedate='2015-04-19',
            priority='H',
            creator_id=1)
        s.save()
        self.assertEqual(Todo_Entries.objects.all().count(), x)

    def test_invalidData(self):
        self.client = Client()
        data = {"description": "too less data"}
        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(response.status_code, 302)
        print("InsertTests executed.")


# testing the behaviour on invalid logins
class Logins(TodoTestCase):
    # valid user (logged in at set-up)
    def test_validLogin(self):
        request = self.client.get('/todolist/overview/login')
        self.assertEqual(request.status_code, 200)

    # invalid user
    def test_unregisteredUser(self):
        self.client.logout()
        self.client.login(username='Aston', password='Martin')
        request = self.client.get('/todolist/overview/login')
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/overview/login',
            status_code=302,
            target_status_code=200)

    # valid user with wrong password
    def test_wrongPassword(self):
        self.client.logout()
        self.client.login(username='testuser', password='Terminator')
        request = self.client.get('/todolist/overview/login')
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/overview/login',
            status_code=302,
            target_status_code=200)


# Testing urls resolving to appropriate views
class UrlTests(TodoTestCase):
    def test_todolist_url_todolist(self):
        resolver = resolve('/todolist/')
        self.assertEqual(resolver.view_name, 'www.todolist.views.overview')

    def test_todolist_url_overview(self):
        resolver = resolve('/todolist/overview/')
        self.assertEqual(resolver.view_name, 'overview')

    def test_todolist_url_add(self):
        resolver = resolve('/todolist/add/')
        self.assertEqual(resolver.view_name, 'add')

    def test_todolist_url_edit(self):
        resolver = resolve('/todolist/edit/1/')
        self.assertEqual(resolver.view_name, 'edit')

    def test_todolist_url_switch(self):
        resolver = resolve('/todolist/switch/1/')
        self.assertEqual(resolver.view_name, 'switch')

    def test_todolist_url_delete(self):
        resolver = resolve('/todolist/delete/1/')
        self.assertEqual(resolver.view_name, 'delete')


# testing all views with valid user and without valid user
class UserViewTest(TodoTestCase):
    def test_overviewView(self):
        request = self.client.get(reverse('overview'))
        self.assertEqual(request.status_code, 200)
        self.assertTemplateUsed(request, 'overview.html')
        self.assertContains(request, 'title', count=2)
        self.client.logout()
        request = self.client.get(reverse('overview'))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/overview/',
            status_code=302,
            target_status_code=200)

    def test_addView_get(self):
        request = self.client.get(reverse('add'))
        self.assertEqual(request.status_code, 200)
        self.assertTemplateUsed(request, 'form.html')
        self.client.logout()
        request = self.client.get(reverse('add'))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/add/',
            status_code=302,
            target_status_code=200)

    def test_addView_post(self):
        response = self.client.post(reverse('add'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form.html')
        self.client.logout()
        response = self.client.post(reverse('add'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/todolist/login/?next=/todolist/add/',
            status_code=302,
            target_status_code=200)

    def test_editView_get(self):
        request = self.client.get(reverse('edit', args={1}))
        self.assertEqual(request.status_code, 200)
        self.assertTemplateUsed(request, 'form.html')
        self.client.logout()
        request = self.client.get(reverse('edit', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/edit/1/',
            status_code=302,
            target_status_code=200)

    def test_editView_post(self):
        response = self.client.post(reverse('edit', args={1}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form.html')
        self.client.logout()
        response = self.client.post(reverse('edit', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/todolist/login/?next=/todolist/edit/1/',
            status_code=302,
            target_status_code=200)

    def test_deleteView_get(self):
        request = self.client.get(reverse('delete', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/overview/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        request = self.client.get(reverse('delete', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/delete/1/',
            status_code=302,
            target_status_code=200)

    def test_deleteView_post(self):
        response = self.client.post(reverse('delete', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/todolist/overview/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        response = self.client.post(reverse('delete', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/todolist/login/?next=/todolist/delete/1/',
            status_code=302,
            target_status_code=200)

    def test_switchView_get(self):
        request = self.client.get(reverse('switch', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/overview/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        request = self.client.get(reverse('switch', args={1}))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/switch/1/',
            status_code=302,
            target_status_code=200)

    def test_switchView_post(self):
        response = self.client.post(reverse('switch', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/todolist/overview/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        response = self.client.post(reverse('switch', args={1}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/todolist/login/?next=/todolist/switch/1/',
            status_code=302,
            target_status_code=200)

    def test_logoutView(self):
        request = self.client.get(reverse('logout'))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/',
            status_code=302,
            target_status_code=200)
        self.client.logout()
        request = self.client.get(reverse('logout'))
        self.assertEqual(request.status_code, 302)
        self.assertRedirects(
            request,
            '/todolist/login/?next=/todolist/logout/',
            status_code=302,
            target_status_code=200)


# testing the deletion of entries in the todolist-DB
class DataDeletionTests(TodoTestCase):
    def test_deleteTodoEntries(self):
        self.assertEqual(Todo_Entries.objects.all().count(), 6)
        self.client.logout()
        self.client.login(username='TodoMaster', password='WiWi')
        self.client.get(reverse('delete', args={1}))
        self.assertEqual(Todo_Entries.objects.all().count(), 5)


# testing the switch-function to change from 'open' to 'done'
class SwitchFunctionTests(TodoTestCase):
    def test_changeStatus(self):
        self.assertEqual(Todo_Entries.objects.filter(status=True).count(), 3)
        self.client.post(reverse('switch', args={1}))
        self.assertEqual(Todo_Entries.objects.filter(status=True).count(), 2)


# testing if non-existing todo-entries throw a 404
class ExistingEntriesTests(TodoTestCase):
    def test_editingNonExistingEntries(self):
        request = self.client.get(reverse('edit', args={25}))
        self.assertEqual(request.status_code, 404)
        response = self.client.post(reverse('edit', args={25}))
        self.assertEqual(response.status_code, 404)

    def test_deletingNonExistingEntries(self):
        request = self.client.get(reverse('delete', args={25}))
        self.assertEqual(request.status_code, 404)
        response = self.client.post(reverse('delete', args={25}))
        self.assertEqual(response.status_code, 404)

    def test_switchNonExistingEntries(self):
        request = self.client.get(reverse('switch', args={25}))
        self.assertEqual(request.status_code, 404)
        response = self.client.post(reverse('delete', args={25}))
        self.assertEqual(response.status_code, 404)
