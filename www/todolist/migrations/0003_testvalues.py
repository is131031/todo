# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def insert_values(apps, schema_editor):
    entry = apps.get_model("todolist", "Todo_Entries")
    db_alias = schema_editor.connection.alias
    entry.objects.using(db_alias).bulk_create([
        entry(
            title="Erster Eintrag",
            description="Beschreibung des ersten Eintrages",
            publication="20150413 18:00:00 PM",
            duedate="2015-04-14",
            duetime="08:00:00 AM",
            priority="H",
            status=0,
            creator_id=1,),
        entry(
            title="Zweiter Eintrag",
            description="Beschreibung des zweiten Eintrages",
            publication="20150413 19:30:00 PM",
            duedate="2015-04-14",
            duetime="08:00:00 AM",
            priority="M",
            status=1,
            creator_id=1,),
        entry(
            title="Dritter Eintrag",
            description="Beschreibung des dritten Eintrages",
            publication="20150413 19:40:00 PM",
            duedate="2015-04-14",
            duetime="08:00:00 AM",
            priority="L",
            status=0,
            creator_id=1,),
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_todo_entries_status'),
    ]

    operations = [
        migrations.RunPython(insert_values,),
    ]
