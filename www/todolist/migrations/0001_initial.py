# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Todo_Entries',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=30)),
                ('description', models.TextField(max_length=500)),
                ('publication', models.DateTimeField(auto_now_add=True)),
                ('duedate', models.DateField()),
                ('duetime', models.TimeField(null=True)),
                ('priority', models.CharField(default=b'L', max_length=1, choices=[(b'H', b'High'), (b'M', b'Medium'), (b'L', b'Low')])),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
