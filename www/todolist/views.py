from django.shortcuts import HttpResponse, get_object_or_404, redirect
from django.template.loader import get_template
from www.todolist.models import Todo_Entries
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.forms.models import ModelForm
from django import forms


class EntryForm(ModelForm):
    class Meta:
        model = Todo_Entries
        fields = ['title', 'description', 'duedate', 'duetime', 'priority']

    duetime = forms.TimeField(required=False)
    duedate = forms.DateField(error_messages={
        'required': 'Datum muss angegeben werden. (Zeit ist optional)'})


@login_required(login_url='django.contrib.auth.views.login')
def overview(request):
    t = get_template('overview.html')
    html = t.render({
        'done_entries': Todo_Entries.objects.filter(
            creator=request.user,
            status=True
            ),
        'open_entries': Todo_Entries.objects.filter(
            creator=request.user,
            status=False
            ),
        })
    return HttpResponse(html)


@login_required(login_url='django.contrib.auth.views.login')
def add(request):
    t = get_template('form.html')

    # Check if it's a POST or a GET-Request
    if request.method == 'POST':
        # For POST-Requests: fill a new form instance with data
        form = EntryForm(request.POST)
        if form.is_valid():
            entry = Todo_Entries()
            entry.title = form.cleaned_data['title']
            entry.description = form.cleaned_data['description']
            entry.duedate = form.cleaned_data['duedate']
            entry.duetime = form.cleaned_data['duetime']
            entry.priority = form.cleaned_data['priority']
            entry.creator = request.user
            entry.save()
            return redirect('overview')
    else:
        # For GET-Requests: create the blank form
        form = EntryForm()

    answer = RequestContext(request, {
        'form': form,
        })

    html = t.render(answer)
    return HttpResponse(html)


@login_required(login_url='django.contrib.auth.views.login')
def edit(request, todo_entry):
    t = get_template('form.html')
    entry = get_object_or_404(Todo_Entries, pk=todo_entry)

    # Check if it's a POST or a GET-Request
    if request.method == 'POST':
        # For POST-Requests: fill a new form instance with data
        form = EntryForm(request.POST)
        if form.is_valid():

            entry.title = form.cleaned_data['title']
            entry.description = form.cleaned_data['description']
            entry.duedate = form.cleaned_data['duedate']
            entry.duetime = form.cleaned_data['duetime']
            entry.priority = form.cleaned_data['priority']
            entry.save()
            return redirect('overview')
    else:
        # For GET-Requests: load entry
        form = EntryForm(instance=entry)

    answer = RequestContext(request, {
        'form': form,
        'todo_entry': todo_entry,
        })

    html = t.render(answer)
    return HttpResponse(html)


@login_required(login_url='django.contrib.auth.views.login')
def delete(request, todo_entry):
    entry = get_object_or_404(Todo_Entries, pk=todo_entry)
    entry.delete()

    return redirect('overview')


@login_required(login_url='django.contrib.auth.views.login')
def switch(request, todo_entry):
    entry = get_object_or_404(Todo_Entries, pk=todo_entry)
    entry.status = not entry.status
    entry.save()
    return redirect('overview')


@login_required(login_url='django.contrib.auth.views.login')
def logout_user(request):
    logout(request)
    return redirect('django.contrib.auth.views.login')
