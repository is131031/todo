from django.db import models


class Todo_Entries(models.Model):
    creator = models.ForeignKey('auth.User')
    title = models.CharField(max_length=30)
    description = models.TextField(max_length=500)
    publication = models.DateTimeField(auto_now_add=True)
    duedate = models.DateField()
    duetime = models.TimeField(null=True)
    HIGH = 'H'
    MEDIUM = 'M'
    LOW = 'L'
    PRIORITY_CHOICES = (
        (HIGH, 'High'),
        (MEDIUM, 'Medium'),
        (LOW, 'Low'),
    )
    priority = models.CharField(
        max_length=1,
        choices=PRIORITY_CHOICES,
        default=LOW)
    status = models.BooleanField("Open/Done", default=False)
