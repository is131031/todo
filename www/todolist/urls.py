from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', "www.todolist.views.overview"),
    url(r'^overview/', "www.todolist.views.overview", name='overview'),
    url(r'^add/', "www.todolist.views.add", name='add'),
    url(r'^edit/(?P<todo_entry>\d+)/$',
        "www.todolist.views.edit", name='edit'),
    url(r'^delete/(?P<todo_entry>\d+)/$',
        "www.todolist.views.delete", name='delete'),
    url(r'^switch/(?P<todo_entry>\d+)/$',
        "www.todolist.views.switch", name='switch'),
    # Login/Logout
    url(r'^login/$', "django.contrib.auth.views.login", name="login"),
    url(r'^logout/$', views.logout_user, name="logout"),
]
