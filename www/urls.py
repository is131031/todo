from django.conf.urls import include, url
from django.contrib import admin
# from todolist import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'www.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^todolist/', include('www.todolist.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^$', views.overview, name='overview'),
]
